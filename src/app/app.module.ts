import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { Network } from '@ionic-native/network';

import { File } from '@ionic-native/file';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { DeportesSubmenuPage } from '../pages/deportes-submenu/deportes-submenu';
import { InformacionesListadoPage } from '../pages/informaciones-listado/informaciones-listado';

import { DeportesEquiposPage } from '../pages/deportes-equipos/deportes-equipos';
import { DeportesGoleadoresPage } from '../pages/deportes-goleadores/deportes-goleadores';
import { DeportesProgramacionPage } from '../pages/deportes-programacion/deportes-programacion';
import { DeportesTablaPage } from '../pages/deportes-tabla/deportes-tabla';

import 'slick-carousel/slick/slick';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HttpProvider } from '../providers/http/http';
import { HttpModule } from '@angular/http';
//import { TooltipModule } from 'ngx-bootstrap/tooltip';

//PIPES
import { LimitToPipe } from '../pipes/limit-to/limit-to';


//NUEVAS PAGES
import { EmprendedoraImportaPage } from '../pages/emprendedora-importa/emprendedora-importa';
import { EmprendedoraEmprenderPage } from '../pages/emprendedora-emprender/emprendedora-emprender';
import { SustentablePuntosPage } from '../pages/sustentable-puntos/sustentable-puntos';
import { SustentablePlazasPage } from '../pages/sustentable-plazas/sustentable-plazas';
import { FutbolSubmenuPage } from '../pages/futbol-submenu/futbol-submenu';
import { DeportesInfraestructuraPage } from '../pages/deportes-infraestructura/deportes-infraestructura';
import { AtractivaPanoramasPage } from '../pages/atractiva-panoramas/atractiva-panoramas';
import { PanoramasDetallePage } from '../pages/panoramas-detalle/panoramas-detalle';
import { AtractivaTuristicosPage } from '../pages/atractiva-turisticos/atractiva-turisticos';
import { ParticipativaListadoPage } from '../pages/participativa-listado/participativa-listado';
import { ParticipativaDetallePage } from '../pages/participativa-detalle/participativa-detalle';
import { SeguraMapsPage } from '../pages/segura-maps/segura-maps';


import { NoticiasListadoPage } from '../pages/noticias-listado/noticias-listado';
import { NoticiasDetallePage } from '../pages/noticias-detalle/noticias-detalle';
import { RiesgosSubmenuPage } from '../pages/riesgos-submenu/riesgos-submenu';
import { RiesgosDetallePage } from '../pages/riesgos-detalle/riesgos-detalle';

//PROVIDERS
import { ApiServicesProvider } from '../providers/api-services/api-services';
import { NetworkProvider } from '../providers/network/network';
import { UtilProvider } from '../providers/util/util';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DeportesSubmenuPage,
    DeportesEquiposPage,
    DeportesGoleadoresPage,
    DeportesProgramacionPage,
    DeportesTablaPage,
    InformacionesListadoPage,
    LimitToPipe,
    EmprendedoraImportaPage,
    EmprendedoraEmprenderPage,
    SustentablePuntosPage,
    SustentablePlazasPage,
    FutbolSubmenuPage,
    DeportesInfraestructuraPage,
    AtractivaPanoramasPage,
    PanoramasDetallePage,
    AtractivaTuristicosPage,
    ParticipativaListadoPage,
    ParticipativaDetallePage,
    NoticiasListadoPage,
    NoticiasDetallePage,
    RiesgosSubmenuPage,
    RiesgosDetallePage,
    SeguraMapsPage
  ],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    //TooltipModule.forRoot(),
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    DeportesSubmenuPage,
    DeportesEquiposPage,
    DeportesGoleadoresPage,
    DeportesProgramacionPage,
    DeportesTablaPage,
    InformacionesListadoPage,
    EmprendedoraImportaPage,
    EmprendedoraEmprenderPage,
    SustentablePuntosPage,
    SustentablePlazasPage,
    FutbolSubmenuPage,
    DeportesInfraestructuraPage,
    AtractivaPanoramasPage,
    PanoramasDetallePage,
    AtractivaTuristicosPage,
    ParticipativaListadoPage,
    ParticipativaDetallePage,
    NoticiasListadoPage,
    NoticiasDetallePage,
    RiesgosSubmenuPage,
    RiesgosDetallePage,
    SeguraMapsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpProvider,
    ApiServicesProvider,
    Network,
    NetworkProvider,
    File,
    UtilProvider,
    InAppBrowser
  ]
})
export class AppModule {}
