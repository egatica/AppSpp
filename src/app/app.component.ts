import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { AlertController } from 'ionic-angular';
import { NetworkProvider } from '../providers/network/network';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = DeportesSubmenuPage;
  rootPage:any = HomePage;

  constructor(platform         : Platform,
              statusBar        : StatusBar,
              splashScreen     : SplashScreen,
              private network  : Network,
              private alertCtrl: AlertController,
              private networkService: NetworkProvider) {
    splashScreen.show();
    platform.ready().then(() => {
      this.networkService.initializeNetworkEvents();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  /*disconnectSubscription = this.network.onDisconnect().subscribe(() => {
    console.log('network was disconnected :-(');
    this.presentAlert();
  });
  connectSubscription = this.network.onConnect().subscribe(() => {
    console.log('network connected!');
    // We just got a connection but we need to wait briefly
     // before we determine the connection type. Might need to wait.
    // prior to doing any api requests as well.
    setTimeout(() => {
      this.presentAlert();
      if (this.network.type === 'wifi') {
        console.log('we got a wifi connection, woohoo!');
      }
    }, 3000);
  });

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Sin conexion a Internet',
      subTitle: 'Volver a conectarse',
      buttons: ['Aceptar']
    });
    alert.present();
  }*/
}

