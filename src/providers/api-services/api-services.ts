/*import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';*/
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
/*
  Generated class for the ApiServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServicesProvider {
  //private api_uri = 'https://jsonplaceholder.typicode.com/postss/1';
  //private api_uri = 'http://gestiondideco.sanpedrodelapaz.cl/noticias/6/index_categoria.json/?page=1';
  //private api_uri:string  = 'http://gestiondideco.sanpedrodelapaz.cl/noticias/{CATEGORIA}/index_categoria.json/?page={PAGE}';
  private api_uri:string;

  constructor(private http: Http) {
    console.log('Hello ApiServicesProvider Provider');
  }

  get(tipo, id, page?): Observable<string[]> {
    console.log("[TIPO,ID,PAGE]",tipo,id,page);
    switch(tipo){
      case 1:
        this.api_uri = `http://gestiondideco.sanpedrodelapaz.cl/noticias/${id}/index_categoria.json/?page=${page}`;
        /*this.api_uri = this.api_uri.replace('{CATEGORIA}',cat);
        this.api_uri = this.api_uri.replace('{PAGE}',page);*/
      break;
      case 2:
        this.api_uri = `http://gestiondideco.sanpedrodelapaz.cl/noticias/${id}/ver.json`;
      break;
    }

    console.log("[GET]",this.api_uri);
    return this.http.get(this.api_uri)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  private extractData(res){
    console.log("[extractdata!!]");
    let body = res.json();
    console.log("[BODY]", body);
    return body || { };
  }

  private handleError(error){
    console.log("[HANDLEERROR]",error);
    return Observable.throw(error);
  }

}
