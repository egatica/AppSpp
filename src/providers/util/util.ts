import { Injectable } from '@angular/core';
import { Http ,Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilProvider {

  constructor(public http: Http) {
    console.log('Hello UtilProvider Provider');
  }

  validarUriGet(uri){
    console.log("[uriririririr]", uri);
    return this.http.get(uri)
              .map(this.extractData)
              .catch(this.handleError);
  }
  private extractData(res){
    console.log("[extractdata!!]");
    let body = res.json();
    console.log("[BODY]", body);
    return body || { };
  }

  private handleError(error){
    console.log("[HANDLEERROR]",error);
    return Observable.throw(error);
  }
}
