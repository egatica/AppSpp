import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesSubmenuPage } from './deportes-submenu';

@NgModule({
  declarations: [
    DeportesSubmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesSubmenuPage),
  ],
})
export class DeportesSubmenuPageModule {}
