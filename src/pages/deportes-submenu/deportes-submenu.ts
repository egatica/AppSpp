import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeportesEquiposPage } from '../deportes-equipos/deportes-equipos';
import { DeportesGoleadoresPage } from '../deportes-goleadores/deportes-goleadores';
import { DeportesTablaPage } from '../deportes-tabla/deportes-tabla';
import { DeportesProgramacionPage } from '../deportes-programacion/deportes-programacion';
import { FutbolSubmenuPage } from '../futbol-submenu/futbol-submenu';
import { DeportesInfraestructuraPage } from '../deportes-infraestructura/deportes-infraestructura';
/**
 * Generated class for the DeporteSubmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deportes-submenu',
  templateUrl: 'deportes-submenu.html'
})
export class DeportesSubmenuPage {
  futSubMenu         = FutbolSubmenuPage;
  depInfraestructura = DeportesInfraestructuraPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeportesSubmenuPage');
  }

}
