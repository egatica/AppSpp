import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacionesDetallePage } from './informaciones-detalle';

@NgModule({
  declarations: [
    InformacionesDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(InformacionesDetallePage),
  ],
})
export class InformacionesDetallePageModule {}
