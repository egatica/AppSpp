import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the InformacionesListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-informaciones-listado',
  templateUrl: 'informaciones-listado.html',
})
export class InformacionesListadoPage {
  page:string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.page = this.navParams.data.page;
  }

  ionViewDidLoad() {
    this.init();
  }
  init(){
    //llamar servicios para informacion
    console.log("thispage ",this.page);
    console.log(this.page);
    /*switch(this.page){
      case "gastronomia":
        alert("gastronomia");
      break;
      case "imperdibles":
      alert("imperdibles");
      break;
      case "rutas":
      alert("rutas");
      break;

    }*/
  }
}
