import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacionesListadoPage } from './informaciones-listado';

@NgModule({
  declarations: [
    InformacionesListadoPage,
  ],
  imports: [
    IonicPageModule.forChild(InformacionesListadoPage),
  ],
})
export class InformacionesListadoPageModule {}
