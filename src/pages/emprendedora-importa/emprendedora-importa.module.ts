import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprendedoraImportaPage } from './emprendedora-importa';

@NgModule({
  declarations: [
    EmprendedoraImportaPage,
  ],
  imports: [
    IonicPageModule.forChild(EmprendedoraImportaPage),
  ],
})
export class EmprendedoraImportaPageModule {}
