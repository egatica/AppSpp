import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesGoleadoresPage } from './deportes-goleadores';

@NgModule({
  declarations: [
    DeportesGoleadoresPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesGoleadoresPage),
  ],
})
export class DeportesGoleadoresPageModule {}
