import { Injector } from '@angular/core';
import { NetworkProvider } from '../../providers/network/network';
import { NavController, LoadingController, ToastController, NavParams,
  AlertController, MenuController, Events } from 'ionic-angular';


export abstract class BasePage {

  public isErrorViewVisible  : boolean;
  public isEmptyViewVisible  : boolean;
  public isContentViewVisible: boolean;
  public isLoadingViewVisible: boolean;
  public networkToast        : any;
  public events              : any;
  public disableActions      : boolean;
  public networkService      : any;
  public loading             : any;
  public alertError          : any;
  protected refresher        : any;
  protected infiniteScroll   : any;
  protected navParams        : NavParams;

  private loader             : any;
  private navCtrl            : NavController;
  private toastCtrl          : ToastController;
  private loadingCtrl        : LoadingController;
  private alertCtrl          : AlertController;


  constructor(injector: Injector) {
    this.loadingCtrl     = injector.get(LoadingController);
    this.toastCtrl       = injector.get(ToastController);
    this.navCtrl         = injector.get(NavController);
    this.alertCtrl       = injector.get(AlertController);
    this.navParams       = injector.get(NavParams);
    this.events          = injector.get(Events);
    this.networkService  = injector.get(NetworkProvider);

    this.disableActions  = this.networkService.previousStatus;

    let menu = injector.get(MenuController);



    this.events.subscribe('network:offline', (e) => {

        this.networkToast = this.toastCtrl.create({
            message: 'No podemos acceder a la red.',
            position: 'bottom',
            duration: 30000,
        });

        this.networkToast.present();
        this.disableActions = true;

    });

    this.events.subscribe('network:online', (e) => {
        this.networkToast.dismiss();
        this.disableActions = false;
    });
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();

    /*setTimeout(() => {
      loading.dismiss();
    }, 5000);*/
  }

  hideLoading(){
    this.loading.dismiss();
  }

  onRefreshComplete(data = null) {

    if (this.refresher) {
      this.refresher.complete()
    }

    if (this.infiniteScroll) {
      this.infiniteScroll.complete();

      if (data && data.length === 0) {
        this.infiniteScroll.enable(false);
      } else {
        this.infiniteScroll.enable(true);
      }
    }
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast"
    });

    toast.present();
  }



  navigateTo(page: any, params: any = {}) {
    this.navCtrl.push(page, params);
  }

  getNavCtrl(){
    return this.navCtrl;
  }

  getAlertCtrl(){
    return this.alertCtrl;
  }

  getToastCtrl(){
    return this.toastCtrl;
  }

}
