import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiesgosDetallePage } from './riesgos-detalle';

@NgModule({
  declarations: [
    RiesgosDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(RiesgosDetallePage),
  ],
})
export class RiesgosDetallePageModule {}
