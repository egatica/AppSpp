import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { DeportesSubmenuPage } from '../deportes-submenu/deportes-submenu';
import { InformacionesListadoPage } from '../informaciones-listado/informaciones-listado';
import { InformacionesMapsPage } from '../informaciones-maps/informaciones-maps';
import { NoticiasListadoPage } from '../noticias-listado/noticias-listado';
import { NoticiasDetallePage } from '../noticias-detalle/noticias-detalle';

//NUEVOS
import { EmprendedoraImportaPage } from '../emprendedora-importa/emprendedora-importa';
import { EmprendedoraEmprenderPage } from '../emprendedora-emprender/emprendedora-emprender';
import { SustentablePuntosPage } from '../sustentable-puntos/sustentable-puntos';
import { SustentablePlazasPage } from '../sustentable-plazas/sustentable-plazas';
import { FutbolSubmenuPage } from '../futbol-submenu/futbol-submenu';
import { AtractivaPanoramasPage } from '../atractiva-panoramas/atractiva-panoramas';
import { AtractivaTuristicosPage } from '../atractiva-turisticos/atractiva-turisticos';
import { ParticipativaListadoPage } from '../participativa-listado/participativa-listado';
import { ParticipativaDetallePage } from '../participativa-detalle/participativa-detalle';
import { RiesgosSubmenuPage } from '../riesgos-submenu/riesgos-submenu';
import { SeguraMapsPage } from '../segura-maps/segura-maps';


//PROVIDERS
import { ApiServicesProvider } from '../../providers/api-services/api-services';
//import * as $ from "jquery";

//window['$'] = window['jQuery'] = $;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  usuarios : any[];
  deportessubmenu           = DeportesSubmenuPage;
  infoListadoPage           = InformacionesListadoPage;
  infoMaps                  = InformacionesMapsPage;
  noticiasListadopage       = NoticiasListadoPage;
  noticiasDetalle           = NoticiasDetallePage;

  //NUEVOS
  empImporta               = EmprendedoraImportaPage;
  empEmprender             = EmprendedoraEmprenderPage;
  susPuntos                = SustentablePuntosPage;
  susPlazas                = SustentablePlazasPage;
  futSubMenu               = FutbolSubmenuPage;
  atrPanoramas             = AtractivaPanoramasPage;
  atrTuristicos            = AtractivaTuristicosPage;
  parListado               = ParticipativaListadoPage;
  parDetalle               = ParticipativaDetallePage;
  segSubMenu               = RiesgosSubmenuPage;
  segMaps                  = SeguraMapsPage;
  /*ngOnInit() {
    $('.box').on('click', function(){
      if($(this).hasClass('open')){
        $(this).removeClass('open');
      }
      else{
        $('.box').removeClass('open');
        $(this).addClass('open');
      }
    });
  }*/

  constructor(public navCtrl: NavController,
              public http: HttpProvider,
              private apiServices: ApiServicesProvider,

            ) {
    //this.cargarUsuarios();
    //this.getApiPrueba()
  }
 /* cargarUsuarios(){
    this.http.get().subscribe( res => {
          this.usuarios = res.results;
          console.log(this.usuarios)
        },
        error =>{
          console.log(error);
        });
  }*/

  /*getApiPrueba(){
    this.apiServices.get('categorias').then((data)=>{
      console.log("[APISERVICES]",data);
    });
  }*/


}
