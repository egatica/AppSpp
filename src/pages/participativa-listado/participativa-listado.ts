import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ParticipativaDetallePage } from '../participativa-detalle/participativa-detalle';
import { ApiServicesProvider } from '../../providers/api-services/api-services';
import { BasePage } from '../base-page/base-page';
/**
 * Generated class for the ParticipativaListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-participativa-listado',
  templateUrl: 'participativa-listado.html',
})
export class ParticipativaListadoPage extends BasePage{
  parDetalle = ParticipativaDetallePage;

  page       :string;
  info       :Array<any>;
  img_default: string = 'assets/imgs/default.png';

  constructor(public navParams   : NavParams,
              public apiService  : ApiServicesProvider,
              public injector    : Injector) {
    super(injector);
    this.page = this.navParams.data.page;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipativaListadoPage');
    this.getInfo();
  }

  getInfo(){
    this.showLoading();
    console.log("[thispage]",this.page);
    //this.apiService.get(this.page,1)
    this.apiService.get(1,6,1)
      .subscribe(
        res=>{
          this.hideLoading();
          console.log("[INIT1]",res);
          this.info = res;

        },
        err => {
          console.log("[INIT2]",err);
          this.showToast("Error en la conexion");
          this.hideLoading();
        });
  }

}
