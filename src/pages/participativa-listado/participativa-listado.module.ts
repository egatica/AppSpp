import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParticipativaListadoPage } from './participativa-listado';

@NgModule({
  declarations: [
    ParticipativaListadoPage,
  ],
  imports: [
    IonicPageModule.forChild(ParticipativaListadoPage),
  ],
})
export class ParticipativaListadoPageModule {}
