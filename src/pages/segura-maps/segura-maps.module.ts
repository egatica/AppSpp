import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeguraMapsPage } from './segura-maps';

@NgModule({
  declarations: [
    SeguraMapsPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguraMapsPage),
  ],
})
export class SeguraMapsPageModule {}
