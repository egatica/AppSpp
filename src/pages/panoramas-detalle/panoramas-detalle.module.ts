import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PanoramasDetallePage } from './panoramas-detalle';

@NgModule({
  declarations: [
    PanoramasDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(PanoramasDetallePage),
  ],
})
export class PanoramasDetallePageModule {}
