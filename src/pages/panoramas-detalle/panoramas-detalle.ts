import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from "jquery";

window['$'] = window['jQuery'] = $;
import { IonicImageViewerModule } from 'ionic-img-viewer';
/**
 * Generated class for the PanoramasDetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-panoramas-detalle',
  templateUrl: 'panoramas-detalle.html',
})
export class PanoramasDetallePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {

    // SLIDER PAGE INFO
    let itemSlide = $('.slider-gallery');
    itemSlide.slick({
      autoplay: true,
      dots: true,
      infinite: true,
      speed: 300,
      fade: true,
      cssEase: 'linear',
      arrows: false,
      pauseOnFocus: false,
      pauseOnHover: false
    });

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PanoramasDetallePage');
  }

}
