import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiServicesProvider } from '../../providers/api-services/api-services';
import { BasePage } from '../base-page/base-page';

/**
 * Generated class for the ParticipativaDetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-participativa-detalle',
  templateUrl: 'participativa-detalle.html',
})
export class ParticipativaDetallePage {
  private id  :number;
  public  info;

  constructor(public navCtrl  : NavController,
              public navParams: NavParams,
              public apiService  : ApiServicesProvider,
              public injector    : Injector) {

    this.id = this.navParams.data.id;
    console.log("[IDPARTICIPATIVA]",this.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipativaDetallePage');
    this.getDetalle();
  }

  getDetalle(){
    console.log("[thispage!!]",this.id);
    //this.apiService.get(this.page,1)
    this.apiService.get(2,this.id)
      .subscribe(
        res=>{
          //this.hideLoading();
          console.log("[RESPONSE]", res);
          console.log("[RESPONSEtypeof]",typeof res);
          this.info = res;



        },
        err => {
          console.log("[INIT2]",err);
          //this.presentAlert();
          //this.hideLoading();
        });
  }

}
