import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParticipativaDetallePage } from './participativa-detalle';

@NgModule({
  declarations: [
    ParticipativaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(ParticipativaDetallePage),
  ],
})
export class ParticipativaDetallePageModule {}
