import { Component, Injector  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NoticiasDetallePage } from '../noticias-detalle/noticias-detalle';
import { ApiServicesProvider } from '../../providers/api-services/api-services';
import { UtilProvider } from '../../providers/util/util';
import { BasePage } from '../base-page/base-page';
import { Response } from '@angular/http';
/**
 * Generated class for the NoticiasListadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticias-listado',
  templateUrl: 'noticias-listado.html',
})
export class NoticiasListadoPage extends BasePage{

  noticiasDetalle   = NoticiasDetallePage;

  noticias   : Array<any>;
  img_default: string = 'assets/imgs/default.png';
  constructor(
              public navParams   : NavParams,
              public apiService  : ApiServicesProvider,
              public injector    : Injector,
              public utilService : UtilProvider) {

    super(injector);
  }

  ionViewDidLoad() {
    this.getNoticias();
    //this.showLoading();
  }

  getNoticias(){
    this.apiService.get(1,6,1)
      .subscribe(
        res=>{
          //this.hideLoading();
          console.log("[INIT1]",res);
          this.noticias = res;

        },
        err => {
          //this.presentAlert();
          //this.hideLoading();
        });
  }

  validImg(uri):boolean{
    return true;
  }



}
