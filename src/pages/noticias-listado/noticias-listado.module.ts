import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiasListadoPage } from './noticias-listado';

@NgModule({
  declarations: [
    NoticiasListadoPage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiasListadoPage),
  ],
})
export class NoticiasListadoPageModule {}
