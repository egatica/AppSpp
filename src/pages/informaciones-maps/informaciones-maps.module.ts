import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacionesMapsPage } from './informaciones-maps';

@NgModule({
  declarations: [
    InformacionesMapsPage,
  ],
  imports: [
    IonicPageModule.forChild(InformacionesMapsPage),
  ],
})
export class InformacionesMapsPageModule {}
