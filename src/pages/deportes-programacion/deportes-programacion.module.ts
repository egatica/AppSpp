import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesProgramacionPage } from './deportes-programacion';

@NgModule({
  declarations: [
    DeportesProgramacionPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesProgramacionPage),
  ],
})
export class DeportesProgramacionPageModule {}
