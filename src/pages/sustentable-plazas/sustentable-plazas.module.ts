import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SustentablePlazasPage } from './sustentable-plazas';

@NgModule({
  declarations: [
    SustentablePlazasPage,
  ],
  imports: [
    IonicPageModule.forChild(SustentablePlazasPage),
  ],
})
export class SustentablePlazasPageModule {}
