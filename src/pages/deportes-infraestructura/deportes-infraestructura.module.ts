import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesInfraestructuraPage } from './deportes-infraestructura';

@NgModule({
  declarations: [
    DeportesInfraestructuraPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesInfraestructuraPage),
  ],
})
export class DeportesInfraestructuraPageModule {}
