import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TuristicoDetallePage } from './turistico-detalle';

@NgModule({
  declarations: [
    TuristicoDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(TuristicoDetallePage),
  ],
})
export class TuristicoDetallePageModule {}
