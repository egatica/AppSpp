import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiasDetallePage } from './noticias-detalle';

@NgModule({
  declarations: [
    NoticiasDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiasDetallePage),
  ],
})
export class NoticiasDetallePageModule {}
