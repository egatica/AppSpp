import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { TurismoDetallePage } from '../turismo-detalle/turismo-detalle';

/**
 * Generated class for the AtractivaTuristicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-atractiva-turisticos',
  templateUrl: 'atractiva-turisticos.html',
})
export class AtractivaTuristicosPage {
  //linkDetalle = TurismoDetallePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AtractivaTuristicosPage');
  }

}
