import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AtractivaTuristicosPage } from './atractiva-turisticos';

@NgModule({
  declarations: [
    AtractivaTuristicosPage,
  ],
  imports: [
    IonicPageModule.forChild(AtractivaTuristicosPage),
  ],
})
export class AtractivaTuristicosPageModule {}
