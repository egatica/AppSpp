import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AtractivaPanoramasPage } from './atractiva-panoramas';

@NgModule({
  declarations: [
    AtractivaPanoramasPage,
  ],
  imports: [
    IonicPageModule.forChild(AtractivaPanoramasPage),
  ],
})
export class AtractivaPanoramasPageModule {}
