import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PanoramasDetallePage } from '../panoramas-detalle/panoramas-detalle';
import { NetworkProvider } from '../../providers/network/network';
/**
 * Generated class for the AtractivaPanoramasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-atractiva-panoramas',
  templateUrl: 'atractiva-panoramas.html',
})
export class AtractivaPanoramasPage {
  linkDetalle = PanoramasDetallePage;
  constructor(public navCtrl: NavController, public navParams: NavParams,  network: NetworkProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AtractivaPanoramasPage');
  }

}
