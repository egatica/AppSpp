import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprendedoraEmprenderPage } from './emprendedora-emprender';

@NgModule({
  declarations: [
    EmprendedoraEmprenderPage,
  ],
  imports: [
    IonicPageModule.forChild(EmprendedoraEmprenderPage),
  ],
})
export class EmprendedoraEmprenderPageModule {}
