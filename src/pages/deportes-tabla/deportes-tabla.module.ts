import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesTablaPage } from './deportes-tabla';

@NgModule({
  declarations: [
    DeportesTablaPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesTablaPage),
  ],
})
export class DeportesTablaPageModule {}
