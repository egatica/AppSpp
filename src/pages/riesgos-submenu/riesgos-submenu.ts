import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { RiesgosDetallePage } from '../riesgos-detalle/riesgos-detalle';
import { File } from '@ionic-native/file';
declare var window;
declare var cordova;

/**
 * Generated class for the RiesgosSubmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-riesgos-submenu',
  templateUrl: 'riesgos-submenu.html',
})
export class RiesgosSubmenuPage {
  detalleRiesgo = RiesgosDetallePage;

  constructor(public navCtrl  : NavController,
              public navParams: NavParams,
              private file    : File,
              private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiesgosSubmenuPage');
  }

  openFile(archivo) {
    let uri: string = `www/assets/${archivo}`;
        window.resolveLocalFileSystemURL(this.file.applicationDirectory +  uri, function(fileEntry) {
          window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {
            fileEntry.copyTo(dirEntry, uri.split('/').pop(), function(newFileEntry) {
              console.log('abriendo',newFileEntry.nativeURL);
              window.open(newFileEntry.nativeURL, '_system');
            });
          });
        });
  }
}
