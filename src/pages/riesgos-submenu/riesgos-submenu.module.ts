import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiesgosSubmenuPage } from './riesgos-submenu';

@NgModule({
  declarations: [
    RiesgosSubmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(RiesgosSubmenuPage),
  ],
})
export class RiesgosSubmenuPageModule {}
