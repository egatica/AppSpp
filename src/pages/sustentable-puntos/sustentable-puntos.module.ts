import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SustentablePuntosPage } from './sustentable-puntos';

@NgModule({
  declarations: [
    SustentablePuntosPage,
  ],
  imports: [
    IonicPageModule.forChild(SustentablePuntosPage),
  ],
})
export class SustentablePuntosPageModule {}
