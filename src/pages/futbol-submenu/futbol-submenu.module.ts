import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FutbolSubmenuPage } from './futbol-submenu';

@NgModule({
  declarations: [
    FutbolSubmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(FutbolSubmenuPage),
  ],
})
export class FutbolSubmenuPageModule {}
