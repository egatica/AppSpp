import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeportesEquiposPage } from '../deportes-equipos/deportes-equipos';
import { DeportesGoleadoresPage } from '../deportes-goleadores/deportes-goleadores';
import { DeportesTablaPage } from '../deportes-tabla/deportes-tabla';
import { DeportesProgramacionPage } from '../deportes-programacion/deportes-programacion';
/**
 * Generated class for the FutbolSubmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-futbol-submenu',
  templateUrl: 'futbol-submenu.html',
})
export class FutbolSubmenuPage {
  depEquipos     = DeportesEquiposPage;
  depGoleadores  = DeportesGoleadoresPage;
  depTabla       = DeportesTablaPage;
  depProgramacion= DeportesProgramacionPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FutbolSubmenuPage');
  }

}
