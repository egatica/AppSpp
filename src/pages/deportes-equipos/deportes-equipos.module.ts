import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeportesEquiposPage } from './deportes-equipos';

@NgModule({
  declarations: [
    DeportesEquiposPage,
  ],
  imports: [
    IonicPageModule.forChild(DeportesEquiposPage),
  ],
})
export class DeportesEquiposPageModule {}
